import java.util.*;
import java.util.Random;
import moonlander.library.*;
import ddf.minim.*;
import processing.opengl.*;



ArrayList<Shape> shapes = new ArrayList<Shape>();
Random random = new Random();
Moonlander moonlander;

int balls = 0;
int spawnInterval = 0;
PShape obu;

void setup()
{
  moonlander = Moonlander.initWithSoundtrack(this, "nils.mp3", 127, 1);
  size(600, 600, P3D);
  for ( int i = 0; i < 60; i++) {
    Shape shape = new Shape((int)random(width-10), (int)random(height-10), (random.nextBoolean() ? -1 : 1), (random.nextBoolean() ? -1 : 1));
    shape.active = true;
    shapes.add(shape);
  }

  obu = loadShape("PiggyBank.obj");
  moonlander.start();
}

void draw() {

  int opacity = (int)moonlander.getValue("background_opacity");
  background(opacity);
  int time = millis();

  moonlander.update();
  int spawn = (int)moonlander.getValue("spawn_shape");
  int mode = (int)moonlander.getValue("mode");
  float pigLight = (float) moonlander.getValue("pig_light");

  if (mode <=1) {
    if ( mode == 1 ) {
      mergeMode(mode);
    }

    for (int i = 0; i < spawn; i++) {
      shapes.get(i).Update(time);
      shapes.get(i).Draw(); 
      curFrameCount = frameCount;
    }
  } else if (mode == 2) {

    Spiral();
    curFrameCount2 = frameCount;
  } else if (mode == 3) {
    colorMode(HSB, 1);
    multiLines(curFrameCount2);
  } else if (mode == 4) {
    colorMode(RGB, 255, 255, 255);
    piggy(opacity, pigLight  );
    exitTime = millis();
  } else {
    piggy(opacity, pigLight  );
    if (millis() - exitTime >10000)
    exit();
  }
}
int exitTime;
float theta1;
float theta2;
void piggy(int opacity, float pigLight){

    background(opacity);
    
    translate(300, 366, 10);
    theta1+=0.05;

    directionalLight(230, 230, 0, 0, 0, pigLight);
    ambientLight(102, 102, 102);
    
    scale(50);
    rotateY(4.5);
    rotateY(millis()*0.0005);
    rotateZ(PI);
    shape(obu);
  
}
int curFrameCount2;
int curTime = 0;
int lastTime = 0;
int numLine = 0;
int numDot = 1000;
float bSize = 100.0;


void multiLines(int curFrameCount) {
  int fc = frameCount - curFrameCount;
  background(0);  
  curTime = millis();
 

  if (curTime - lastTime >1000)
  {
    if (numLine <10) {
      numLine = numLine + 1;
      lastTime = curTime;
    }
  }

  for ( int j=0; j< numLine; j++) {
    float hue_incr = fc * .001 + .1;
    float hue = hue_incr*j;
    hue -= Math.floor(hue);
    fill( hue-noise(j/1.5), 100-noise(j/0.1), 100*noise(j/0.5));

    beginShape();
    stroke(155,0,255);
    float f1 = noise(j/10.0)+0.5;
    float f2 = noise(j/7.0);  
    float f3 = noise(j/1.3)+.5;
    float f4 = noise(j/1.2, fc/500.0);
    float f5 = noise(j/5.0);
    float f6 = noise(j/1.8)+.9;
    int step = (int)bSize ;
    for ( int i=0; i<numDot; i+=int (step)) {
      ellipse(i, height/2+(800*f4)*sin(f1*TWO_PI*i/600 + 100*f2 - fc/(100.0*f3) + 150*f5 * 100.0*f6), bSize, bSize);
    }
    endShape();
  }
  if (numLine == 10.0) {
    if (bSize > 1.5) bSize -=0.2;
  } 
  for ( int j=0; j< numLine; j++) {
    float hue_incr = fc * .001 + .1;
    float hue = hue_incr*j;
    hue -= Math.floor(hue);
    fill( 0, 0, 0);

    beginShape();
    noStroke();
    float f1 = noise(j/10.0)+0.5;
    float f2 = noise(j/7.0);  
    float f3 = noise(j/1.3)+.5;
    float f4 = noise(j/1.2, fc /500.0);
    float f5 = noise(j/5.0);
    float f6 = noise(j/1.8)+.9;
    int step = (int)bSize ;
    for ( int i=0; i<numDot; i+=int (step)) {
      ellipse(i, height/2+(800*f4)*sin(f1*TWO_PI*i/600 + 100*f2 - fc/(100.0*f3) + 150*f5 * 100.0*f6), bSize, bSize);
    }
    endShape();
  }
}


int curFrameCount;
int   nbr_circles = 75;
float angle_incr = 2*PI / nbr_circles;
int dim = 0;
void drawSpiral(int col, int curFrameCount) {
  fill(col);
  float elapsedSeconds = millis()*0.001;
  float angle_incr = radians(2 + (frameCount-curFrameCount)/16.0);

  float cx = width/2;
  float cy = height/2;
  float outer_rad = width*.45;

  float sm_diameter = dim;

  for (int i = 1; i <= nbr_circles; ++i) {
    float ratio = i/(float)nbr_circles;
    float spiral_rad = ratio * outer_rad;
    float angle = i*angle_incr;
    float x = cx + cos(angle) * spiral_rad;
    float y = cy + sin(angle) * spiral_rad;

    // draw tiny circle at x,y
    ellipse(x, y, sm_diameter, sm_diameter);
  }
}
void Spiral() {
  dim +=4;
  stroke(2);
  strokeWeight(1);
  drawSpiral(255, curFrameCount);
}

static class CONFIG {  
  public static boolean MERGE = false;
  public static int VELOCITY = 2;
}

void mergeMode(int value) {
  CONFIG.MERGE = true;
  CONFIG.VELOCITY = value*2;
}

class PointXY {
  float x, y;
  color c;
  PointXY(float pX, float pY, color curColor) {
    x = pX;
    y = pY;
    c = curColor;
  }
}

class Shape
{
  float x;
  float y;
  float velocity;

  float xSign;
  float ySign;

  int size;
  int maxSize;

  boolean active;
  color curColor;

  LinkedList<PointXY> trailPoints;

  Shape(int startX, int startY, int signX, int signY)
  {
    x = (float)startX;
    y = (float)startY;

    xSign = signX;
    ySign = signY;


    this.colorPicker();

    maxSize = (int)random(10, 20);
    size = 0;

    active = true;
    trailPoints = new LinkedList<PointXY>();
  }  

  void Draw() {   
    for (int i = trailPoints.size ()-1; i >= 0; --i) {

      noStroke();
      fill(trailPoints.get(i).c, 150-i*5);
      ellipse(trailPoints.get(i).x, trailPoints.get(i).y, size, size);
    }
  }

  void Update(int time) {

    CollisionCheck();

    if ( size < maxSize ) {
      size += 10;
    } else if ( size > maxSize ) {
      size -= 10;
    }

    x += xSign * noise(x*frameCount)*1.8 * CONFIG.VELOCITY;
    y += ySign * noise(y*frameCount)*1.8 * CONFIG.VELOCITY;

    if ( !CONFIG.MERGE && trailPoints.size() > 50) {
      trailPoints.removeLast();
    } else if ( CONFIG.MERGE )
    {
      trailPoints.clear();
    }

    trailPoints.addFirst(new PointXY(x, y, curColor));
  }

  void CollisionCheck() {

    for (int i=0; i < shapes.size (); i++) {
      if (shapes.get(i) != this && shapes.get(i).active) {
        Shape cShape = shapes.get(i);
        float cDist = dist(x, y, cShape.x, cShape.y);
        float colDist = cDist - ((size+cShape.size) / 2);

        if ( colDist < 0) {

          if ( !CONFIG.MERGE ) { 

            // Bounce shape logic
            cShape.xSign = cShape.xSign * -1;
            cShape.ySign = cShape.ySign * -1;
            xSign = xSign * -1;
            ySign = ySign * -1;
          } else {

            // Shape merge logic
            if ( this.size >= cShape.size ) {
              this.maxSize += cShape.size;
              cShape.maxSize = 0;
            } else {
              cShape.maxSize += this.size;
              this.maxSize = 0;
            }
          }

          // Color switch logic
          if ( cShape.curColor == this.curColor ) {
          } else {
            cShape.curColor = this.curColor;
          }
        }
      }
    }

    if ( y - size/2 <= 0 ) {
      ySign = 1;
      this.colorPicker();
    } else if (y + size/2 >= height) {
      ySign = -1;
      this.colorPicker();
    }

    if ( x - size/2 <= 0 ) {
      xSign = 1;
      this.colorPicker();
    } else if ( x + size/2 >= width ) {
      xSign = -1;
      this.colorPicker();
    }
  }

  private void colorPicker() {
    int colorPicker = (int)random(1, 4);

    switch(colorPicker) {
      case(1):
      curColor = color(random(100, 255), 0, 0);
      break;
      case(2):
      curColor = color(0, random(100, 255), 0);
      break;  
      case(3):
      curColor = color(0, 0, random(100, 255));
      break;
    };
  }
}

