float x,y,z;
int bg_clr;

Spinner spinner1;
Spinner spinner2;

void setup() {
  size(400,400,P3D);
  bg_clr = 255;
  spinner1 = new Spinner(0,0);
  spinner2 = new Spinner(0,0);
}

void draw() {
  int time = 60 * millis() / 1000;
  rectMode(CENTER);
  
  if(bg_clr == 0){
     bg_clr = 255; 
  }
  
  bg_clr = bg_clr - time;
    
  background(time,0,0);
  
  
  spinner1.Draw(bg_clr, time);
  spinner2.Draw(bg_clr+50, -time);
}

class Spinner{
  
  int _x;
  int _y;
  
  Spinner(int x, int y){ 
    _x = x;
    _y = y;
  }
  
  void Draw(int bg, int time){
    fill(bg);
    translate(200,200,0);
    rotateZ(time*PI/128);
    rotateY(time*PI/128);
    rotateX(time*PI/128);
    rect(_x,_y,200,200);
    translate(-200,-200,0);
  }
}
 

